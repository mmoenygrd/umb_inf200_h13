# Exercises Due Tuesday October 4th #

## Guidelines ##

1. You will find six exercises below. You can hand in as many exercises as you like, but you well get credit for at most two exercises per week.
1. Deadline for delivery is **Friday, October 4th, noon**.
1. How to work on your exercises under Git control:
    1. Create a repository for all your exercises on Bitbucket
    1. Clone the repository to your computer, the computer lab computer, or both
    1. Using Eclipse, 
        1. create an `ExDue1004` branch, branching off your `master` branch
        1. create a Python project `ExDue1004` in that branch
    1. Inside the project, create a Python module for each exercise you want to work on. I suggest you use filenames `ex07.py`, `ex08.py`, etc.
    1. Write your code.
    1. Commit changes often. 
    1. If you work on different computers (e.g. laptop and computer lab computer), remember to push your commits to every time you stop working on one computer and to pull changes before you start working on the other.
1. How to hand in:
	1. Once you are satisfied with your work, make a last commit.
	1. Push your code to the Bitbucket server.
	1. Create a pull request for the `ExDue1004` branch with your review partner and `heplesser` as reviewers
1. Evaluation:
	1. Comment on the code your review partner has submitted and click `Approve` when you are done.

## Exercises ##

### Exercise 7 ###

Write a program that implements the [Sieve of
Erastosthenes](http://en.wikipedia.org/wiki/Sieve_of_Eratosthenes)
method for finding all prime numbers up to a given number N. Your
program should print at least the prime numbers up to N.

### Exercise 8 ###

Write a program that tabulates the development of the amount of money in a savings account over time:

 1. At the beginning, you have, e.g., 1.000.000 in the account.
 1. At the end of each year, the following happens:
     1. The banks pays interest into the account, e.g., 2%.
     1. Income tax is deducted on the interest paid by the bank, e.g., 28% of the interest paid.
     1. On the money now in the account, property tax is deducted, e.g., 1.1% of the total amount in the account.
     1. You withdraw a certain amount of money, e.g., 50.000 (same amount each year).
 1. For each year, print
     - the year
     - the interest paid
     - the income tax paid
     - the property tax paid
     - the amount withdrawn
     - the amount remaining in the account
     - the amount remaining in the account adjusted for inflation, e.g., 1.5%

Note that, while this example is based on Norwegian tax rules, it does not include any tax free allowances and thus imposes too high taxes.

### Exercise 9 ###

Write a program that does the following three things:

1. reads a list of numbers and uses a single list comprehension to print a list that contains only those numbers that are divisible by 3;
1. reads a list of words and uses a single list comprehension to print a list that contains only those words that are palindromes, i.e., identical spelled forward or backward (lower case);
1. reads a list of words and uses a single list comprehension to print a list that contains only those words that begin with 'A' or 'a' and not four letters long.


### Exercise 10 ###

Write a program that evaluates the following function for any value the user supplies. The program should loop, asking for new values, until the user just presses `ENTER`.

              /-
              |     x / ( x + 1 )         x < -10
              |     sin(x)              -10 < x <= -1
    f(x) =   <      x^2                  -1 < x < 1
              |     20                    x = 1
              |     ln(x+1)               1 < x 
              \-



### Exercise 11 ###

Write a program providing recommendation for ski preparation. The program should ask the user for the temperature and the type of snow (e.g., 'new', 'old', 'icy' ...) and then recommends what wax to apply. The program should loop until the user indicates that she want to quit the program. 



### Exercise 12 ###

Write a program that reads a list of numbers and prints the numbers in ascending order. The program must **not** use the built-in sort functions. For each loop, specify an invariant.

