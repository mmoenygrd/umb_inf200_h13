# Exercises Due Friday November 8th #

## Guidelines ##

1. You will find six exercises below. You can hand in as many exercises as you like, but you well get credit for at most two exercises per delivery (three exercises per delivery from 25 Oct onwards).
1. Deadline for delivery is **Friday, November 8th, 15.00**.
1. How to work on your exercises under Git control:
    1. Create a repository for all your exercises on Bitbucket
    1. Clone the repository to your computer, the computer lab computer, or both
    1. Using Eclipse, 
        1. create an `ExDue1108` branch, branching off your `master` branch
        1. create a Python project `ExDue1108` in that branch
    1. Inside the project, create a Python module for each exercise you want to work on. I suggest you use filenames `ex25.py`, `ex26.py`, etc.
    1. Write your code.
    1. Commit changes often. 
    1. If you work on different computers (e.g. laptop and computer lab computer), remember to push your commits to every time you stop working on one computer and to pull changes before you start working on the other.
1. How to hand in:
	1. Once you are satisfied with your work, make a last commit.
	1. Push your code to the Bitbucket server.
	1. Create a pull request for the `ExDue1108` branch with your review partner and `heplesser` as reviewers
1. Evaluation:
	1. Comment on the code your review partner has submitted and click `Approve` when you are done.
	1. As of this exercise, you can earn credit for up to three exercises per week.

## Exercises ##

### Exercise 25

Write a Python program that tabulates mathematical functions for
values given by the user. The program should tabulate at least sine,
logarithm and square root, but you are encouraged to add more
functions.

The program shall ask the user for the values at which the functions
are to be tabulated. All values shall be converted to floats. Any
input that cannot be converted to floats shall be reported below the
table. If a user enters a value for that a given function is not
defined (e.g. sqrt(-2)), then print "n/a" in the table, as in the
example below:

    In [3]: run tabulating.py
    Enter data: -2 3 d 
    Enter data: 0
    Enter data: 
             x    sin(x)    log(x)   sqrt(x)
    ----------------------------------------
        -2.000    -0.909       n/a       n/a
         0.000     0.000       n/a     0.000
         3.000     0.141     1.099     1.732
    ----------------------------------------
    
    Non-numeric inputs:  ['d']

You table should be formatted tidily.


### Exercise 26

Define a `SingleWalk` class that simulates a single walk home, as in
Exercise 22. The constructor should receive a `Walker` instance as
argument and have a `simulate()` method which simulates the walk
home. After `simulate()` has run, the `number_of_steps()` method
should return the number of steps the walker needed to get home.

You should write `number_of_steps()` so that it either throws an
exception if called before `simulate()` has run, or that it
automatically runs `simulate()` if it has not yet been run. How can
you detect whether `simulate()` has run? 

Perform some test simulations using the `SingleWalk` class.

### Exercise 27

In Exercise 22, you defined a `Walker` class, in which a random walker
took steps to the left and the right with equal probability. Define a
subclass `BiasedWalker` for walkers that step to the left with
probability p and to the right with probability 1-p. The probability
should be passed to the `BiasedWalker` constructor and is constant
during the lifetime of the walker.

Using either the code from the main program of Exercise 22, or,
preferably, the `SingleWalk` class from Exercise 26, run some
experiments on how p affects the time to get home, compared to
Exercise 22.

### Exercise 28

This exercise is also based on the `Walker` class from Exercise
22. Here you shall define a subclass `TiringWalker`, in which the
walker takes steps according to the following rules:

1. With probability q(steps) = 1-exp(-steps/lambda), the walker does *not* move.
1. If the walker moves, it moves with equal probability to left or
right.
1. steps is only increased when the walker moves.
1. The walker "dies" if q(steps) < q_min.

The purpose of the last condition is that we need to avoid that the
simulation "freezes" as q(steps) becomes very small with growing step count.
lambda and q_min are constants larger than 0, which should be passed
to the constructor of the `TiringWalker`. 

Run some experiments as in Exercise 27 and see how lambda and q_min
affect the time to get home.


### Exercise 29

In exercises 22, 26, 27, and 28, you run individual simulations of
random walks. Usually, we want to collect statistics across a large
number of walks, to see how walking lengths are distributed. Write a
class `Experiment` that runs a given number of `SingleWalk`
simulations and collects the walk durations observed. Note that you
will need a new `Walker` and `SingleWalk` class for each single walk.

It is fine if you implement this class for only one of the `Walker`
classes from the previous experiments. But if you want, you are
encouraged to think about ways to make the `Experiment` class
flexible, so that it can be used with any of the `Walker` subclasses
(even subclasses we may only write in the future). The challenge here
is that you need to create a new
`Walker`/`BiasedWalker`/`TiringWalker`/... instance for each single walk,
and that each of those classes needs a different set of constructor
arguments.



### Exercise 30

Extend the stack-based calculator we developed in class to support
more mathematical functions, such as log, tan, ....

