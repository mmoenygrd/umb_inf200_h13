# Exercises Due Friday November 29th #

## Guidelines ##

1. You will find six exercises below. You can hand in as many exercises as you like, but you well get credit for at most two exercises per delivery (three exercises per delivery from 25 Oct onwards).
1. Deadline for delivery is **Friday, November 29th, 15.00**.
1. This will be the last set of exercises in the fall parallel. You can get up to **six** exercises approved from this set.
1. How to work on your exercises under Git control:
    1. Create a repository for all your exercises on Bitbucket
    1. Clone the repository to your computer, the computer lab computer, or both
    1. Using Eclipse, 
        1. create an `ExDue1129` branch, branching off your `master` branch
        1. create a Python project `ExDue1129` in that branch
    1. Inside the project, create a Python module for each exercise you want to work on. I suggest you use filenames `ex25.py`, `ex26.py`, etc.
    1. Write your code.
    1. Commit changes often. 
    1. If you work on different computers (e.g. laptop and computer lab computer), remember to push your commits to every time you stop working on one computer and to pull changes before you start working on the other.
1. How to hand in:
	1. Once you are satisfied with your work, make a last commit.
	1. Push your code to the Bitbucket server.
	1. Create a pull request for the `ExDue1129` branch with your review partner and `heplesser` as reviewers
1. Evaluation:
	1. Comment on the code your review partner has submitted and click `Approve` when you are done.

## Exercises ##


### Exercise 31

The following code defines a generator and uses it to create a list of
Fibonacci numbers F_0 through F_10.

    def FiboGen(maxNumber):
        "Fibonacci generator returning F_0, F_1, ... F_maxNumber."

        a, b = 0, 1
        while maxNumber >= 0:
            yield a
            a, b = b, a + b
            maxNumber -= 1

    print list(FiboGen(10))

Modify this code to create a `FactorialGen` generator, so that 

    print list(FactorialGen(N))

will print a list of 0!, 1!, ..., N! for any integer N.


### Exercise 32

Write a Python Module `mmatrix` that provides a class `Matrix`. This
class should implement basic operations for two-dimensional matrices,
at least addition and multiplication of two matrices, and
multiplication of a matrix with a scalar. The class should also
provide suitable `__repr__` and `__str__` classes. A matrix should be
created like this

    m = Matrix([[1, 2, 3], [4, 5, 6]])

for a matrix with two rows and three columns.

Hint: To allow access to matrix elements like this

    print m[1, 2]
    m[1, 2] = 22

you need to define methods `__getitem__(self, key)` and
`__setitem__(self, key, value)`. `__getitem__()` gets called automatically when `m[]` 
appears in any Python expression, while `__setitem__()` gets called automatically
when `m[]` appears on the left side of an assignment; see, e.g. Hetland p 182ff. To find out how
these methods work, just define them inside a test class and let them
print their arguments!


### Exercise 33

The board game [Chutes &
Ladders](http://en.wikipedia.org/wiki/Chutes_and_ladders)
("Stigespillet") is based on a random walk on a board as follows:

1. The player starts at position 0.
1. For each move, the player rolls a die and between 1 and 6 positions
forward.
1. If the player then is at the foot of a ladder, the player
immediately jumps to the top of the ladder.
1. If the player then is at the top of a chute, the player immediately
jumps to the bottom of the chute.
1. The game ends once the player reaches or passes position 90.

The board (Norwegian edition) has the following

- Ladders: 1 -> 40, 8 -> 10, 36 -> 52, 43 -> 62, 49 -> 79, 65 -> 82, 68 -> 85
- Chutes: 24 -> 5, 33 -> 3, 42 -> 30, 56 -> 37, 64 -> 27, 74 -> 12, 87 -> 70

You shall

1. modify or subclass the `Walker` class to implement this random walk
1. modify or subclass the `SingleWalk` class to implement the game
1. collect statistics on game durations

### Exercise 34

Write a proper testsuite for the `Stack` class implemented in
`stack.py`! Start from the tests in the main-section of `stack.py` and
convert them into `unittest` test cases. Place all tests in file
`test_stack.py`.

### Exercise 35

Write a testsuite for the `Calc` class implemented in `calc.py`. Place
all tests in file `test_calc.py`.

### Exercise 36

Write a testsuite for the `Matrix` class, placing all tests in file
`test_mmatrix.py`.
