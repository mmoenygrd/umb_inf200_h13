# Exercises Due Tuesday September 20th #

## Guidelines ##

1. You will find six exercises below. You can hand in as many exercises as you like, but you well get credit for at most two exercises per week.
1. Deadline for delivery is **postponed** to **Friday, September 27th, noon**.
1. How to work on your exercises under Git control:
    1. Create a repository for all your exercises on Bitbucket
    1. Clone the repository to your computer, the computer lab computer, or both
    1. Using Eclipse
        1. create a branch `ExDue0920`
        1. create a Python project `ExDue0920` in that branch
    1. Inside the project, create a Python module for each exercise you want to work on. I suggest you use filenames `ex01.py`, `ex02.py`, etc.
    1. Write your code.
    1. Commit changes often. 
    1. If you work on different computers (e.g. laptop and computer lab computer), remember to push your commits to every time you stop working on one computer and to pull changes before you start working on the other.
1. How to hand in:
	1. Once you are satisfied with your work, make a last commit.
	1. Push your code to the Bitbucket server.
        1. Give `heplesser`, `andreasflo` and your review partner read-access to the repository
	1. Create a pull request for the `ExDue1004` branch with your review partner and `heplesser` as reviewers
1. Evaluation:
	1. Comment on the code your review partner has submitted and click `Approve` when you are done.

## Exercises ##

### Exercise 1 ###

For each of the cases below, write as compact Python code as possible that transforms the original list into a new list so that the `print` statement gives the result shown.
"Compact" means here that you should use as few statements as possible.

#### Case A ####
```
orig_list = list('hello')
# your code
print new_list
--> ['o', 'e', 'l', 'l', 'o']
```

#### Case B ####
```
orig_list = [1, 2, 3, 4, 5]
# your code
print orig_list, new_list
--> [1, 2, 3, 4, 5] [1, 3, 5]
```

#### Case C ####
```
orig_list = [67, 23, 54, 28, 19]
#your code
print orig_list, new_list
--> [67, 23, 54, 28, 19] [19, 28, 53, 54, 67]
```


### Exercise 2 ###

For each of the cases below, write as compact Python code as possible that transforms the original dictionary into a new dictionary so that the `print` statement gives the result shown.
"Compact" means here that you should use as few statements as possible.

#### Case A ####
```
orig_dict = {0: 1, 1: 1, 2: 2, 3: 3, 4: 5}
#your code
print orig_dict, new_dict
--> {0: 1, 1: 1, 2: 2, 3: 3, 4: 5} {0: 1, 1: 1, 3: 3, 4: 5}
```

#### Case B ####
```
orig_dict = {0: 1, 1: 1, 2: 2, 3: 3, 4: 5}
#your code
print orig_dict, new_dict
--> {0: 1, 1: 1, 2: 2, 3: 3, 4: 5} {0: 1, 1: -1, 2: 2, 3: 3, 4: 5, 5:8}
```

#### Case C ####
```
orig_dict = {0: 1, 1: 1, 2: 2, 3: 3, 4: 5}
#your code
print orig_dict, new_dict
--> {0: 1, 1: 1, 2: 2, 3: 6, 4: 5} {0: 1, 1: 1, 2: 2, 3: 6, 4: 5} 
```


### Exercise 3 ###

Write a program that asks the user to enter names and phone numbers and then allows the user to query numbers.
To make things easier, you can set up the program so that the user gets to enter a fixed number of names and numbers, and that names contain no spaces.

### Exercise 4 ###

Write a program containing a predefined mapping from names to phone numbers and that allows reverse lookup, i.e., that the user can enter a number and get out the name.

### Exercise 5 ###

Write a program that reads names and numbers, and then prints an alphabetical list of names and numbers.

### Exercise 6 ###

Write a program that can handle first name, last name, phone number and room number. The program should allow lookup by last name, and the user should be able to specify which information he or she wants, e.g.:
```
Last name: Plesser
Info wanted: first
--> Hans
```
