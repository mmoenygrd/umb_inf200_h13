# Exercises Due Friday October 25th #

## Guidelines ##

1. You will find six exercises below. You can hand in as many exercises as you like, but you well get credit for at most two exercises per delivery (three exercises per delivery from 25 Oct onwards).
1. Deadline for delivery is **Friday, October 25th, 15.00**.
1. How to work on your exercises under Git control:
    1. Create a repository for all your exercises on Bitbucket
    1. Clone the repository to your computer, the computer lab computer, or both
    1. Using Eclipse, 
        1. create an `ExDue1025` branch, branching off your `master` branch
        1. create a Python project `ExDue1025` in that branch
    1. Inside the project, create a Python module for each exercise you want to work on. I suggest you use filenames `ex07.py`, `ex08.py`, etc.
    1. Write your code.
    1. Commit changes often. 
    1. If you work on different computers (e.g. laptop and computer lab computer), remember to push your commits to every time you stop working on one computer and to pull changes before you start working on the other.
1. How to hand in:
	1. Once you are satisfied with your work, make a last commit.
	1. Push your code to the Bitbucket server.
	1. Create a pull request for the `ExDue1025` branch with your review partner and `heplesser` as reviewers
1. Evaluation:
	1. Comment on the code your review partner has submitted and click `Approve` when you are done.
	1. As of this exercise, you can earn credit for up to three exercises per week.

## Exercises ##

### Exercise 19

The bisection algorithm treated in Exercise 15 is implemented using
recursion, which is probably not too efficient. Assume that the
function has exactly zero in the search interval.

1. Implement the same algorithm using a loop. 
1. Extend the argument list so that the user can
specifiy the precision required to decide that the zero has been
found. 
1. How does the number of function evaluations change with the precision required?


### Exercise 20

Create a class for a phone book entry. The constructor should accept
the following information and store it in the instance:

- first name
- last name
- phone number

The class should have a `display()` method that prints a phonebook
entry in nice form. In the main program, create several objects of the
class, and print a list like this (here joe and jane are the objects
of the class

    for entry in [joe, jane]:
        entry.display()

gives the following output

    Joe Doe's number is 1234
    Jane Dane's number is 5678

### Exercise 21

Extend the class from Exercise 20 as follows: In addition to being
able to construct a phone book entry from three arguments, the
constructor should also be able to contruct a phone book entry from a
single string containing first name, last name and phone number (in
that order), separated by spaces (we assume single first and last
names only). I.e., you should be able to create entries like this:

    PhoneBookEntry('Jane', 'Dane', '5467')
    PhoneBookEntry('Joe Doe 1234')

The main program in you module shall create several entries using
either way and print the as for Exercise 20.


### Exercise 22

Simulate a student's way home to Pentagon after a hard night at
Samfunnet.  The student is a walker in a one-dimensional world. At
each point in time, the student is at a position `x` and steps to the
left (`x-1`) or right (`x+1`) with equal probability. The students
starts at some `x0`, and stops when he or she arrives home at position
`h`. Create a class `Walker` with a constructor and methods to take
one step, to check whether the student is at home, and to access the
students current position. In the main program, ask the user for the
initial position, the position of the student's home, and report how
many steps the student needed to get home, compared to the direct
distance. Use either `random.randint()` or `random.uniform()` to get
the necessary random numbers.

Example result:

    Joe's starting point: 0
    Joe's home: 10
    Joe arrived at home after 46 steps. Direct distance: 10.

### Exercise 23

Modify the simulation of exercise 22 to simulate the process of two
students meeting each other: Create two `Walker` objects with
different initial positions and let them take steps until they are in
the same position. 

Important: If, as in exercise 22, each student moves either one
position to the left or one to the right on each step, you must make
sure that the difference in initial positions is an even number, or
the students will never meet (why?). Alternatively, you can change the
walking rule so that the students move either `-1`, `0`, or `+1`,
i.e., that they can stay in place. Example result:

   Joe's starting point: -5
   Jane's starting point: 5
   Joe met Jane at position 2 after 68 steps. Direct distance 10 steps.

As a challenge, visualise the walks either as they happen or once they
are complete.

### Exercise 24

Based on Exercise 22, implement a ["Gambler's ruin"](http://en.wikipedia.org/wiki/Gambler's_ruin) simulation. The
gambler's ruin problem is as follows:

At the beginning of the game, the gambler has `n` dollars (kroners,
coins, whatever ...), while the bank has `m` dollars, so the total
amount of money available is `N=n+m`.  At each turn of the game, a coin
is flipped. With probability 1/2, the coin shows heads and the gambler
wins one dollar from the bank; otherwise, the gambler loses one dollar
to the bank.  The game ends when the gambler either goes broke (`n==0`)
or breaks the bank (`n==N`).  We are interested in how often the gambler
goes broke and breaks the bank, and in how long the games last,
depending on the initial values for `n` and `m`. To collect statistics, we
perform an experiment consisting of many games.

Your program should define three classes (representing the gambler, a
single game, and an experiment). Within an experiment, a user-defined
number of games should be played, and the durations for lost and won
games be recorded. At the end of the experiment, results are displayed
in text form or as graphics. 

What happens if the coin is not fair, i.e., the probability of heads
is not 1/2?

For a thorough mathematical treatment, see e.g. Ch. XIV in William Feller, *An
Introduction to Probability Theory and Its Applications*, Vol. 1, 3rd
ed, John Wiley & Sons, New York, 1970, or [this
website](http://www.mathpages.com/home/kmath084/kmath084.htm).

