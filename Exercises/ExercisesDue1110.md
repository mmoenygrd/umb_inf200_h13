# Exercises Due Friday October 11th #

## Guidelines ##

1. You will find six exercises below. You can hand in as many exercises as you like, but you well get credit for at most two exercises per week.
1. Deadline for delivery is **Friday, October 11th, 15.00**.
1. How to work on your exercises under Git control:
    1. Create a repository for all your exercises on Bitbucket
    1. Clone the repository to your computer, the computer lab computer, or both
    1. Using Eclipse, 
        1. create an `ExDue1011` branch, branching off your `master` branch
        1. create a Python project `ExDue1011` in that branch
    1. Inside the project, create a Python module for each exercise you want to work on. I suggest you use filenames `ex07.py`, `ex08.py`, etc.
    1. Write your code.
    1. Commit changes often. 
    1. If you work on different computers (e.g. laptop and computer lab computer), remember to push your commits to every time you stop working on one computer and to pull changes before you start working on the other.
1. How to hand in:
	1. Once you are satisfied with your work, make a last commit.
	1. Push your code to the Bitbucket server.
	1. Create a pull request for the `ExDue1011` branch with your review partner and `heplesser` as reviewers
1. Evaluation:
	1. Comment on the code your review partner has submitted and click `Approve` when you are done.

## Exercises ##

### Exercise 13

1. Implement a loop-based function computing the n-th Fibonacci number F_n, where F_0 = 0, F_1 = 1 and F_n = F_{n-1} + F_{n-2} for n >= 2. The function should return only the n-th number.
1. Write a second Fibonacci function that uses recursion instead of a loop to compute the Fibonacci numbers.
1. Test run times for both Fibonacci functions for successively larger arguments. You can do this with the `timeit` function in iPython

        In [2]: timeit math.sin(0.3)
        10000000 loops, best of 3: 146 ns per loop

1. Include the results of your timing experiments in a comment in your code.

### Exercise 14

This exercise extends exercise 13. 

1. Write a second recursive Fibonacci function that remembers all Fibonacci numbers that it has calculated so far (what would be a good data structure for this?). Then, when you need, e.g., F_3 while computing F_5, and the value of F_3 is in memory, you can just look it up and use it, instead of computing it again from scratch.
1. Use `timeit` to compare the performance of this function with memory to the versions from Ex. 13. Include the results in a comment in the code.

Hint: You will need two functions, one that sets up the memory data structure and then calls the actual recursive "worker" function. You should pass the memory data structure to the worker function, and the worker function is allowed to modify the memory data structure (this is one of few cases where we need a side effect).


### Exercise 15

In the exercises directory, you will find a file `ex15.py`, which implements the bisection method to find the zero of a function. Unfortunately, it has some bugs. Fix them so that the program will work properly. You should test it for more cases than the one included in the sample program. Click here to download the code in raw format.

### Exercise 16

Write a function that takes a nested list (arbitrary depth) and returns a fully flattened list, i.e., a list containing the same elements in the same order, but all at the same level, e.g.

    [1, 2, [3, [4, 5]], [6, 7]]  --> [1, 2, 3, 4, 5, 6, 7]

Assume that the lists only contains numbers (Things get a bit more complicated if the list could contain strings and we would not flatten the strings). 

**Try to solve this on your own first, without googling!**

### Exercise 17

One of the oldest (and worst) pseudo-random-number generators is [von
Neumann's middle square method](http://en.wikipedia.org/wiki/Middle-square_method). 

Assume we want 4-digit random numbers. Then we start out with, e.g.,
4567 as our first number (the seed). We then square it to obtain
20857489 and now take the middle four digits of this number, i.e.,
8574 as our next random number. We repeat this as long as we need more
random numbers. The number of requested digits must be even.

1. Write a function that takes two arguments: the number of digits and a random number, and returns the next random number in the sequence. For example

        print middlesquare(4, 4567)
        8574

1. Write a function that, given a number of digits and a starting value, generates one random number after the other until a number is repeated. The function should return the cycle length, i.e., the number of random numbers between the first and second occurence of the number (see below for examples).
1. Write a program that determines sequence lengths for all possible four-digits starting values and reports the distribution of sequence lengths.

Example for 2-digit numbers:

    22 -> 48 -> 30 -> 90 -> 10 -> 10  => sequence length 1
    24 -> 57 -> 24 => sequence length 2

#### Hints

1. Use integer arithmetic to get rid of the extra digits at the left and right of the center four digits.
1. Use a dictionary to map random numbers that you have seen to the position in the sequence when they first occurred. Use this dictionary to check whether a number has been seen before, and when.
1. Use a dictionary with sequence lengths as keys to collect statistics on sequence lengths.


### Exercise 18

Implement two random number generator classes. Both classes shall have methods 

- `init()` to initialize the generator
- `rand()` to return the next random number

`rand()` should not take any arguments except `self`. 

One of the classes shall implement a linear congruential generator (LCG), which generates numbers according to the following equation

    r[n+1] = a * r[n] mod m

where `a = 7**5 = 16807` and `m = 2**31-1`. For this class, `init()` takes a single integer, r0 the seed value, as argument.

The other class shall be based on a list of random numbers: For this class, `init()` takes a list of numbers, and `rand()` returns the first number from that list when it is called for the first time, the second number when called the second time, etc. The numbers could, e.g., be obtained by rolling dice, by reading off the last digits of phone numbers in the phone book or the stock market pages.

The main program shall instantiate at least one generator of each class and print a few numbers from each.

