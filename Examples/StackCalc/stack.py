"""
The stack module provides a basic stack implementation.

"""

__author__ = "Hans E Plesser"
__email__ = "hans.ekkehard.plesser@umb.no"


class StackUnderflow(Exception):
    """Raised if too few elements are found on the stack."""

    def __init__(self, msg=None):
        """
        msg --- details about the cause of the exception
        """

        self.msg = msg

    def __str__(self):
        return str(self.msg)


class Stack(object):
    """
    Implements a basic stack with a minimal set of operations.

    The stack may contain any kind of Python objects.
    """

    def __init__(self):
        """Initialize as empty stack."""

        self.data = []

    def push(self, value):
        """Push given value on stack."""

        self.data.append(value)

    def pop(self):
        """Remove topmost element from stack and return it."""

        if len(self.data) < 1:
            raise StackUnderflow('Cannot pop from empty stack.')

        return self.data.pop()

    def exch(self):
        """Exchange the order of the two topmost stack elements."""

        if len(self.data) < 2:
            raise StackUnderflow('At least two elements required on stack.')

        self.data[-2:] = self.data[:-3:-1]

    def empty(self):
        """Returns True if the stack contains no data."""

        return len(self.data) == 0

    def height(self):
        """Returns number of elements on the stack."""

        return len(self.data)


if __name__ == '__main__':

    # Several tests for stacks follow
    s = Stack()
    assert s.empty(), "New stack not empty"
    assert s.height() == 0, "New stack height not zero"

    s.push(11)
    assert not s.empty(), "Stack empty after push"

    s.push(22)
    s.push(33)
    assert s.height() == 3, "Wrong stack height"

    r = s.pop()
    assert r == 33, "Wrong value popped"
    assert s.height() == 2, "Stack did not shrink on pop"

    # stack should now be [11 22]
    s.exch()
    # stack should now be [22 11]
    assert s.height() == 2, "Stack height changed during exch"
    assert s.pop() == 11, "Wrong top element after exch"
    assert s.pop() == 22, "Wrong second element after exch"

    assert s.empty(), "Stack not empty after popping all elements"

    # ensure that exch does not change lower-lying stack elements
    for n in range(10):
        s.push(n)
    s.exch()
    assert s.pop() == 8, "Top element wrong"
    assert s.pop() == 9, "Second element wrong"
    for n in range(7, -1, -1):
        assert s.pop() == n, "Element {} wrong".format(n)
    assert s.empty(), "Stack not empty after popping all elements"

    # ensure that exch works for mutable stack elements, too
    a = [1, 2, [3, 4]]
    b = [5, 6, [7, 8]]
    s.push(a)
    s.push(b)
    s.exch()
    assert s.pop() == a, "Top mutable elem wrong after exch"
    assert s.pop() == b, "Second mutable elem wrong after exch"

    # stack is now empty, ensure pop throws StackUnderflow
    try:
        s.pop()
    except StackUnderflow:
        # exception raised as desired, nothing to do
        pass
    else:
        # no exception raised, force failed assertion
        assert False, "pop on empty stack did not raise StackOverflow"

    # same test for exch with single element on stack
    s.push(1)
    try:
        s.exch()
    except StackUnderflow:
        # exception raised as desired, nothing to do
        pass
    else:
        # no exception raised, force failed assertion
        assert False, "exch on single-element stack didn't raise StackOverflow"

    print "All tests passed."
