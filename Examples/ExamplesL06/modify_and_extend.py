'''
Idea 3: Modifying and extending data types.
'''

class Member(object):
    def __init__(self, name, number):
        self.name, self.number = name, number

    def display(self):
        print "Member: {0.name} (#{0.number})".format(self)
        
class Officer(Member):
    def __init__(self, name, number, rank):
        Member.__init__(self, name, number)
        self.rank = rank
        
    def display(self):
        print "{0.rank}: {0.name} (#{0.number})".format(self)
        
if __name__ == '__main__':
    
    club = [Officer('Joe', 1, 'President'),
            Officer('Jane', 2, 'Treasurer'),
            Member('Jack', 3)]
    
    for person in club:
        person.display()
        
    per = Member('Per', 5)
    per.display()
    Member.display(per)
    
    ida = Officer('Ida', 6, 'Webmaster')
    ida.display()
    Member.display(ida)
    Officer.display(ida)
    
