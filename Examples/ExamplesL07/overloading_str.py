'''
Overloading __str__ and __repr__
'''

class Member(object):
    def __init__(self, name, number):
        self.name, self.number = name, number
        
    def __str__(self):
        return "Member: {0.name} (#{0.number})".format(self)

    def __repr__(self):
        return "Member('{0.name}', {0.number})".format(self)

    def display(self):
        print self
        
class Officer(Member):
    def __init__(self, name, number, rank):
        Member.__init__(self, name, number)
        self.rank = rank

    def __str__(self):
        return "{0.rank}: {0.name} (#{0.number})".format(self)
        
    def __repr__(self):
        return "Officer('{0.name}', {0.number}, '{0.rank}')".format(self)
        
        
if __name__ == '__main__':
    
    club = [Officer('Joe', 1, 'President'),
            Officer('Jane', 2, 'Treasurer'),
            Member('Jack', 3)]

    print club
    
    for person in club:
        print person
