'''
Rectangle class using vectors
This versions uses the property-based vector implementation
using lists internally.
'''

from vector_prop_list import Vector
        
class Rectangle(object):
    
    def __init__(self, ll, ur):
        
        # ensure we store corners as vectors
        self._ll = ll if isinstance(ll, Vector) else Vector(*ll)
        self._ur = ur if isinstance(ur, Vector) else Vector(*ur)
        
    def area(self):
        diag = self._ur - self._ll
        return diag.x * diag.y

    def circumference(self):
        diag = self._ur - self._ll
        return 2 * ( diag.x + diag.y )
        
if __name__ == '__main__':
    
    r1 = Rectangle((0.5, 0.5), (3, 2))
    r2 = Rectangle(Vector(0.5, 0.5), Vector(3, 2))
    
    print r1.area(), r1.circumference()
    print r2.area(), r2.circumference()
