'''
This vector implementation is based on storing coordinates in a list.

It is mostly meant to illustrate how one can change the implementation
while retaining the interface when using properties.
'''

import math

class Vector(object):
    
    def __init__(self, x, y):
        self._coords = [x, y]

    def getX(self): return self._coords[0]

    def setX(self, x): self._coords[0] = x

    def getY(self): return self._coords[1]

    def setY(self, y): self._coords[1] = y

    x = property(getX, setX)
    y = property(getY, setY)

    def __repr__(self):
        return 'Vector({0.x}, {0.y})'.format(self)

    def __add__(self, rhs):
        return Vector(self._coords[0] + rhs._coords[0],
                      self._coords[1] + rhs._coords[1])

    def __sub__(self, rhs):
        return Vector(self._coords[0] - rhs._coords[0],
                      self._coords[1] - rhs._coords[1])

    def __mul__(self, rhs):
        return Vector(self._coords[0] * rhs,
                      self._coords[1] * rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __div__(self, rhs):
        return self * (1 / rhs)

    def norm(self):
        return math.sqrt(self._coords[0]**2
                         + self._coords[1]**2)


if __name__ == '__main__':

    v = Vector(1, 2)
    w = Vector(30, 40)

    print "v       = ", v 
    print "w       = ", w
    print "v + w   = ", v + w
    print "2 * v   = ", 2 * v 
    print "v * 5   = ", v * 5 
    print "v / 10. = ", v / 10.
    
