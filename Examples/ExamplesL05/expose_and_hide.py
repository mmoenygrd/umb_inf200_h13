'''
Illustrate Idea 2: Expose interface, hide implementation
'''

import math
import combining        
        
class Rectangle(object):
    def __init__(self, lower_left, upper_right):
        self.l_l = lower_left
        self.width = upper_right[0] - lower_left[0]
        self.height = upper_right[1] - lower_left[1]
        
    def area(self):    
        return self.width * self.height
        
if __name__ == '__main__':
    
    shapes = [          Rectangle((0.5, 0.5), (3, 2)),
              combining.Rectangle((0.5, 0.5), (3, 2))]
    
    for shape in shapes:
        print shape.area()
