'''
This version of the 2D vector class uses properties
'''

import math


class Vector(object):

    def __init__(self, x, y):
        self._x = x
        self._y = y

    def getX(self):
        return self._x

    def setX(self, x):
        self._x = x

    def getY(self):
        return self._y

    def setY(self, y):
        self._y = y

    x = property(getX, setX)
    y = property(getY, setY)

    def __repr__(self):
        return 'Vector({0._x}, {0._y})'.format(self)

    def __add__(self, rhs):
        return Vector(self._x + rhs._x, self._y + rhs._y)

    def __sub__(self, rhs):
        return Vector(self._x - rhs._x, self._y - rhs._y)

    def __mul__(self, rhs):
        return Vector(self._x * rhs, self._y * rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __div__(self, rhs):
        return self * (1 / rhs)

    def norm(self):
        return math.sqrt(self._x ** 2 + self._y ** 2)


if __name__ == '__main__':

    v = Vector(1, 2)
    w = Vector(30, 40)

    print "v       = ", v
    print "w       = ", w
    print "v + w   = ", v + w
    print "2 * v   = ", 2 * v
    print "v * 5   = ", v * 5
    print "v / 10. = ", v / 10.
    print "norm(v) = ", v.norm()
