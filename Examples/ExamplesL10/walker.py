'''
Created on Nov 5, 2013

@author: plesser
'''

import random


class Walker(object):
    '''
    classdocs
    '''

    def __init__(self, start, home):

        self.start = start
        self.home = home
        self.position = start
        self.stepsTaken = 0

    def atHome(self):
        return self.position == self.home

    def takeStep(self):
        if random.randint(0, 1) == 0:
            self.position += 1
        else:
            self.position -= 1
        self.stepsTaken += 1

    def goHome(self):
        while not self.atHome():
            self.takeStep()

    def getNumberOfSteps(self):
        return self.stepsTaken


if __name__ == '__main__':

    students = [Walker(0, 20) for _ in range(10)]

    for student in students:
        student.goHome()
        print student.getNumberOfSteps()
