"""Illustrate comparison overloading"""


class A(object):
    def __init__(self, x):
        self._x = x

    def __eq__(self, rhs):
        return self._x == rhs._x


class B(object):
    def __init__(self, x):
        self._x = x

    def __eq__(self, rhs):
        return self._x == rhs._x

    def __lt__(self, rhs):
        return self._x < rhs._x

    def __ne__(self, rhs):
        return not (self == rhs)

    def __le__(self, rhs):
        return (self < rhs) or (self == rhs)

    def __ge__(self, rhs):
        return not (self < rhs)

    def __gt__(self, rhs):
        return not (self <= rhs)

if __name__ == '__main__':

    # class without rich comparison implemented
    aa = A(10)
    ab = A(10)

    # surprise ...
    print "aa == ab: ", aa == ab, ", aa != ab: ", aa != ab
    print id(aa), id(ab)

    # class with rich comparison implemented
    b1, b1_, b2 = B(1), B(1), B(2)

    # everything below should print True
    print     b1 == b1_, not b1 == b2
    print not b1 != b1_, b1 != b2
    print not b1 < b1, b1 < b2, not b2 < b1
    print     b1 <= b1_, b1 <= b2, not b2 <= b1
    print     b1 >= b1_, not b1 >= b2, b2 >= b1
    print not b1 > b1_, not b1 > b2, b2 > b1
