'''
This version of the 2D vector class uses properties

Illustrates read-only properties
'''

import math


class Vector(object):

    def __init__(self, x, y):
        self._x = x
        self._y = y

    def getX(self):
        return self._x
    x = property(getX)

    # other way of defining read-only properties: use a decorator
    @property
    def y(self):
        return self._y

    def __repr__(self):
        return 'Vector({0._x}, {0._y})'.format(self)

    def __add__(self, rhs):
        return Vector(self._x + rhs._x, self._y + rhs._y)

    def __sub__(self, rhs):
        return Vector(self._x - rhs._x, self._y - rhs._y)

    def __mul__(self, rhs):
        return Vector(self._x * rhs, self._y * rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __div__(self, rhs):
        return self * (1 / rhs)

    def norm(self):
        return math.sqrt(self._x ** 2 + self._y ** 2)


if __name__ == '__main__':

    v = Vector(1, 2)
    print v.x, v.y
    v.x = 10  # error
