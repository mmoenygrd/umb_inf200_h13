'''A basic 2d-vector class.'''

import math


class Vector(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return 'Vector({0.x}, {0.y})'.format(self)

    def __add__(self, rhs):
        return Vector(self.x + rhs.x, self.y + rhs.y)

    def __sub__(self, rhs):
        return Vector(self.x - rhs.x, self.y - rhs.y)

    def __mul__(self, rhs):
        return Vector(self.x * rhs, self.y * rhs)

    def __rmul__(self, lhs):
        return self * lhs

    def __div__(self, rhs):
        return self * (1 / rhs)

    def norm(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)


if __name__ == '__main__':

    v = Vector(1, 2)
    w = Vector(30, 40)

    print "v       = ", v
    print "w       = ", w
    print "v + w   = ", v + w
    print "2 * v   = ", 2 * v
    print "v * 5   = ", v * 5
    print "v / 10. = ", v / 10.
    print "norm(v) = ", v.norm()
