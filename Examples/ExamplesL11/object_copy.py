'''
A simple example illustrating copying of objects.

@author: plesser
'''

import copy


class A(object):
    pass

a = A()
a.x = 10
b = a
b.x = 20
print a.x, b.x  # -> 20 20

c = copy.copy(a)
c.x = 50
print a.x, b.x, c.x  # -> 20 20 50

a.l = [1, 2]
c = copy.copy(a)  # a.l, c.l bound to same list object
print id(a.l), id(c.l)
c.l[1] = 10  # change list
print a.l, c.l  # -> [1, 10] [1, 10]

d = copy.deepcopy(c)
d.l[0] = 5
print c.l, d.l  # -> [1, 10] [5, 10]
print id(a.l), id(c.l), id(d.l)
