"""
Simple file I/O illustrations.
"""


def ex_read():
    f = open('text.txt', 'r')
    for line in f:
        print line
    f.close()


def ex_write():
    f = open('wtest.txt', 'w')
    for line in ['ab', 'cd']:
        f.write(line + '\n')
    f.close()


def ex_append():
    f = open('atest.txt', 'a')
    for line in ['ab', 'cd']:
        f.write(line + '\n')
    f.close()


def read_all_at_once():
    f = open('text.txt')
    lines = f.readlines()
    print len(lines)
    for line in lines:
        print line.upper()
    f.close()


def read_one_at_a_time():
    f = open('text.txt')
    for line in f:
        print line.upper()
    f.close()


def compact_one_at_a_time():
    for line in open('text.txt'):
        print line.upper()


def write_try_finally():
    f = open('wtest.txt', 'w')
    try:
        f.write('text')
    finally:
        f.close()


def write_with():
    with open('wtest.txt', 'w') as f:
        f.write('withtext')
