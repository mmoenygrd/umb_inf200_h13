'''
An example of a generator for Fibonacci numbers and the equivalent
iterator.
'''


class FibsIter(object):
    def __init__(self, maxValue):
        self._a = 0
        self._b = 1
        self._maxV = maxValue

    def __iter__(self):
        return self

    def next(self):
        self._a, self._b = self._b, self._a + self._b
        if self._a > self._maxV:
            raise StopIteration
        return self._a


def FibsGen(maxValue):
    a, b = 1, 1
    while a < maxValue:
        yield a
        a, b = b, a + b

# explicit use
fi = FibsIter(10)
print "Iterator : ", fi.next(), fi.next(), fi.next()

fg = FibsGen(10)
print "Generator: ", fg.next(), fg.next(), fg.next()

# "implicit" use
print "Iterator : ", list(FibsIter(10))
print "Generator: ", list(FibsGen(10))

print "Iterator : ", sum(FibsIter(100))
print "Generator: ", sum(FibsGen(100))
