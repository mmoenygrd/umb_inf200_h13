'''
An example of a generator for Fibonacci numbers generating a
given number of Fibonacci numbers.
'''


def FiboGen(maxNumber):
    "Fibonacci generator returning F_0, F_1, ... F_maxNumber."

    a, b = 0, 1
    while maxNumber >= 0:
        yield a
        a, b = b, a + b
        maxNumber -= 1

if __name__ == '__main__':

    for nmax in range(11):
        print 'maxNumber = {:>2} => {}'.format(nmax, list(FiboGen(nmax)))
