'''
Example of iterator where __iter__() returns not just self.
'''


class MyList(object):

    class _Iterator(object):
        def __init__(self, data):
            self._data = data
            self._idx = -1

        def next(self):
            self._idx += 1
            if self._idx >= len(self._data):
                raise StopIteration
            return self._data[self._idx]

    def __init__(self, data):
        self._data = data

    def __iter__(self):
        return MyList._Iterator(self._data)


if __name__ == '__main__':

    ml = MyList(['a', 'b', 'c', 'd'])

    for i1 in ml:
        for i2 in ml:
            print i1, i2
