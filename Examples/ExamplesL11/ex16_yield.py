'''
Module providing a flattening function using yield.
'''

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def flatten(nested):
    """
    Returns a flattened version of nested. nested must be a list.
    """

    def _flatten(nested):
        for item in nested:
            if isinstance(item, (list, tuple)):
                for subitem in _flatten(item):
                    yield subitem
            else:
                yield item

    return list(_flatten(nested))

if __name__ == '__main__':

    print flatten([1, 2, 3, 4])
    print flatten([1, 2, [3, [4, 5], 'abc'], [6, 7]])
    print flatten([])
