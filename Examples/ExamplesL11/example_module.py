#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Example module.

This is an example module to illustrate how to write modules.

Written at Ås.
"""
__version__ = '0.1'
__author__ = 'Hans Ekkehard Plesser, UMB'


def print_greeting(name):
    """Print greeting.

    Function prints greeting including given name."""

    print "Hello, %s!" % name

if __name__ == '__main__':
    you = raw_input('What is your name: ')
    print_greeting(you)
