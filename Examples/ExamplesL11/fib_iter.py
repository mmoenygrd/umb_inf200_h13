'''
An iterator for Fibonacci numbers.

Based on Hetland, Beginning Python, p 193
'''


class Fibs(object):
    """This Fibs iterator does not stop."""

    def __init__(self):
        self._a = 0
        self._b = 1

    # Method called next() must return next value
    # in sequence
    def next(self):
        self._a, self._b = self._b, self._a + self._b
        return self._a

    # Method __iter__() must return object implementing
    # next() method. By providing an __iter__ method, the
    # class indicates that it is iterable.
    def __iter__(self):
        return self


class FibsStopped(Fibs):
    """This Fibs iterator generates a given number of numbers."""

    def __init__(self, maxNumbers=None):
        super(FibsStopped, self).__init__()
        self._numbers_to_do = maxNumbers

    def next(self):
        if self._numbers_to_do <= 0:
            raise StopIteration

        self._numbers_to_do -= 1
        return super(FibsStopped, self).next()

if __name__ == '__main__':

    fibs = Fibs()
    for f in fibs:
        print f
        if f > 100:
            break

    print list(FibsStopped(maxNumbers=10))
