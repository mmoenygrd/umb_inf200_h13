"""
Palindrome testing module.
"""

__author__ = 'HEP'
__email__ = 'hans.ekkehard.plesser@umb.no'


def is_palindrome(text):
    utext = text.lower()
    return utext == utext[::-1]
