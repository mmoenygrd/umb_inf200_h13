'''
Created on Nov 19, 2013

@author: plesser
'''


def fib(n):

    if n % 1 != 0:
        raise ValueError('F_n only defined on ints.')

    if n < 0:
        raise ValueError('F_n undefined for n < 0')
    elif n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for _ in range(2, n + 1):
            a, b = b, a + b
        return b

if __name__ == '__main__':
    pass
