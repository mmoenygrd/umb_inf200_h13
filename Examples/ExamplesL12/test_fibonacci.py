'''
Created on Nov 19, 2013

@author: plesser
'''
import unittest

import fibonacci as fb


class Test(unittest.TestCase):

    def testCall(self):
        fb.fib(0)

    def testFib0(self):
        self.assertEqual(fb.fib(0), 0, 'F_0 != 0')

    def testFib1(self):
        self.assertEqual(fb.fib(1), 1, 'F_1 != 1')

    def testFib2(self):
        self.assertEqual(fb.fib(2), 1, 'F_2 != 1')

    def testFib3(self):
        self.assertEqual(fb.fib(3), 2, 'F_3 != 2')

    def testFibRecursion(self):
        for n in range(2, 100):
            self.assertEqual(fb.fib(n),
                             fb.fib(n - 1) + fb.fib(n - 2),
                             'F_{} != F_{} + F_{}'.format(n, n - 1, n - 2))

    def testNegArguments(self):
        self.assertRaises(ValueError, fb.fib, -1)

    def testNonIntArguments(self):
        self.assertRaises(ValueError, fb.fib, 1.5)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testCall']
    unittest.main()
