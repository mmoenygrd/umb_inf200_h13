"""
Random walk simulation

This program simulates a random symmetric 1D random walk
from a starting point to a target.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import random


class Walker(object):
    """
    Single walker object.

    The only property of the walker is the one-dimensional position.
    """

    def __init__(self, x0):
        """Initialize with initial position."""

        self.x = x0
        self.steps = 0

    def getPosition(self):
        """Returns current position."""

        return self.x

    def getNumSteps(self):
        """Returns number of steps the walker has taken."""

        return self.steps

    def isAt(self, position):
        """Returns True if walker is at given position."""

        return self.x == position

    def step(self):
        """
        Change coordinate by +1 or -1 with equal probability.
        """

        self.x += 2 * random.randint(0, 1) - 1
        self.steps += 1


if __name__ == '__main__':

    start = int(raw_input("Joe's starting point: "))
    home = int(raw_input("Joe's home          : "))

    joe = Walker(start)
    while not joe.isAt(home):
        joe.step()

    message = "Joe arrived at home after {} steps. Direct distance: {}."
    print message.format(joe.getNumSteps(), abs(start - home))
