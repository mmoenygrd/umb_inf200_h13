"""
This module provides classes representing two-dimensional shapes.

This version illustrates how to catch exceptions.

This code builds on ex33.py.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import math
import sys


class NoInputError(Exception):
    """Error raised if user supplies no input."""

    def __init__(self, message=None):
        self.msg = message

    def __str__(self):
        return str(self.msg)


class Circle(object):
    """A Circle."""

    def __init__(self, center, radius):
        """
        center - 2-element tuple with center coordinates, (x, y)
        radius - circle radius
        """

        if radius < 0:
            raise ValueError('Radius must be non-negative')

        self.ctr = center
        self.rad = radius

    def area(self):
        """
        Returns area of the circle.
        """

        assert self.rad >= 0, \
               "Inconsistent object: negative radius"

        return math.pi * self.rad ** 2

    def circumference(self):
        """
        Returns circumference of the circle.
        """

        assert self.rad >= 0, \
               "Inconsistent object: negative radius"

        return 2 * math.pi * self.rad


def read_circle_specs():
    """Returns center coordinates and radius as x, y, r."""

    data = raw_input("Enter circle data (x y r): ")
    if not data.strip():
        raise NoInputError

    x, y, r = (float(d) for d in data.split())
    return x, y, r


if __name__ == '__main__':

    circles = []
    while True:
        try:
            x, y, r = read_circle_specs()
            circles.append(Circle((x, y), r))
        except NoInputError:
            break
        except ValueError as err:
            print "You entered an unsuitable value: ", err
            print "Please try again!"
        except Exception as err:
            print "Something went badly wrong, aborting.", err
            sys.exit(-1)

    for c in circles:
        print "{:10.2f} {:10.2f}".format(c.rad, c.area())
