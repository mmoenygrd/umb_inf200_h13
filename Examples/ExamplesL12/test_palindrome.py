'''
Created on Oct 24, 2012

@author: plesser
'''

import unittest

import palindrome


class Test(unittest.TestCase):

    def testCall(self):
        palindrome.is_palindrome('anna')

    def testLowercasePositive(self):
        self.assertTrue(palindrome.is_palindrome('anna'))
        self.assertTrue(palindrome.is_palindrome('ana'))

    def testLowercaseNegative(self):
        self.assertFalse(palindrome.is_palindrome('per'))
        self.assertFalse(palindrome.is_palindrome('lisa'))

    def testUppercase(self):
        self.assertTrue(palindrome.is_palindrome('ANNA'))
        self.assertTrue(palindrome.is_palindrome('ANA'))
        self.assertFalse(palindrome.is_palindrome('PER'))
        self.assertFalse(palindrome.is_palindrome('LISA'))

    def testMixedCase(self):
        self.assertTrue(palindrome.is_palindrome('Anna'),
                        'Mixed-case palindrome not detected.')
        self.assertFalse(palindrome.is_palindrome('Lisa'))

    def testEmpty(self):
        self.assertTrue(palindrome.is_palindrome(''))

if __name__ == "__main__":
    unittest.main(verbosity=2)
