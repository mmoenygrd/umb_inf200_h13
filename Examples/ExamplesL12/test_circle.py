"""
Unittest suite for circle class.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import unittest
import math

import circle_catch as cc


class TestCircle(unittest.TestCase):
    """
    Tests for circle class.
    """

    def test_creation(self):
        "Test that we can create a circle."
        cc.Circle((0, 0), 1)

    def test_radius(self):
        "Test that circle has radius attribute with correct value."
        c = cc.Circle((0, 1), 2)
        self.assertEqual(c.rad, 2)

    def test_center(self):
        "Test that circle has center attribute with correct value."
        c = cc.Circle((0, 1), 2)
        self.assertEqual(c.ctr, (0, 1))

    def test_zero_radius(self):
        "Test that zero radius gives zero area and circumference."
        c = cc.Circle((1, 2), 0)
        self.assertEqual(c.area(), 0)
        self.assertEqual(c.circumference(), 0)

    def test_area(self):
        "Test area calculation for non-zero radii."
        c1 = cc.Circle((0, 0), 1)
        self.assertAlmostEqual(c1.area(), math.pi)
        c2 = cc.Circle((0, 0), 10)
        self.assertAlmostEqual(c2.area(), math.pi * 100)

    def test_circumference(self):
        "Test circumference calculation for non-zero radii."
        c1 = cc.Circle((0, 0), 1)
        self.assertAlmostEqual(c1.circumference(), 2 * math.pi)
        c2 = cc.Circle((0, 0), 10)
        self.assertAlmostEqual(c2.circumference(), 20 * math.pi)

    def test_reject_neg_radius(self):
        "Test that constructor rejects negative radius."

        self.assertRaises(ValueError, cc.Circle, (0, 1), -1)


if __name__ == '__main__':

    unittest.main(verbosity=2)
