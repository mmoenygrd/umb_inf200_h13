# INF200 Advanced Programming

This repository contains all material for INF200 *Advanced Programming* in the Fall 2013.

You will find lecture slides (INF200_H13_Lxx.pdf files) and other documentation under the "Downloads" tab.

Hans E Plesser

