"""
Performance comparison of loop- and recursion-based Fibonacci numbers routines.

Results on MacBook Pro, 2.53 GHz i5, OSX 10.6.8, Python 2.7.3

N     Loop      Recursion     Recursion with memory
5     0.9 us       2.6 us          4.4 us
10    1.2 us      30.0 us          8.2 us
20    2.2 us    3700.0 us         16.0 us
40    3.3 us       ---            32.0 us
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def fib_loop(n):
    '''
    Returns n-th Fibonacci number.

    Code is adapted from Hetland, Beginning Python, p 115.
    '''

    if n < 2:
        return n
    else:
        n0, n1 = 0, 1
        for _ in xrange(n - 1):
            n0, n1 = n1, n0 + n1
        return n1


def fib_rec(n):
    '''
    Returns n-th Fibonacci number.

    This is a straightforward recursive implementation.
    '''

    if n < 2:
        return n
    else:
        return fib_rec(n - 1) + fib_rec(n - 2)


def fib_rec_mem(n):
    '''
    Returns n-th Fibonacci number.

    This implementation caches Fibonacci numbers that have been computed.
    '''

    def fib_work(n, known_fibs):
        '''
        Returns n-th Fibonacci number.

        This is a internal worker function that does the actual computation.

        known_fibs is a dictionary mapping integers to their respective
        Fibonacci numbers. It must contain F_0 and F_1.
        Note: known_fibs is modified during the function invocation. When
        fib_work returns, known_fibs will contain all Fibonacci numbers up
        to F_n.
        '''

        assert 0 in known_fibs and 1 in known_fibs, \
            "known_fibs must contain F_0 and F_1."

        if n not in known_fibs:
            known_fibs[n] = (fib_work(n - 1, known_fibs)
                              + fib_work(n - 2, known_fibs))

        return known_fibs[n]

    known_fibs = {0: 0, 1: 1}

    return fib_work(n, known_fibs)


if __name__ == '__main__':

    TABLE_FORMAT = "{:>4}{:>12}{:>12}{:>12}"
    print "Fibonacci numbers"
    print TABLE_FORMAT.format("N", "F_N (loop)", "F_N (rec)", "F_N (mem)")
    for n in xrange(10):
        print TABLE_FORMAT.format(n, fib_loop(n), fib_rec(n), fib_rec_mem(n))
