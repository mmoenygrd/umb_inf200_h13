"""
Program illustrating polymorphism by way of random number generator classes.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


class ListRNG(object):
    """
    Random number generator providing numbers from list.
    """

    def init(self, numbers):
        """
        Initialize generator with list of numbers.

        Note:
        - These numbers will be returned as random numbers, so they should come
          from a good source of randomness.
        - If more numbers are requested than are in the loop, the same sequence
          is returned over and over again.
        """

        self._random_numbers = numbers
        self._next = 0

    def rand(self):
        """
        Return next random number.
        """
        r = self._random_numbers[self._next]
        self._next += 1
        if self._next >= len(self._random_numbers):
            self._next = 0
        return r


class LCG(object):
    """
    Linear congruential random number generator.

    Numbers are obtained from r_{n+1} = a r_m mod m
    with a = 7**5 and m = 2**31-1 as suggested by
    Park & Miller (Comm ACM 31:1192-1201, 1988).

    This generator is reasonable, but the sequence
    length too short by todays standards.
    """

    a = 7 ** 5
    m = 2 ** 31 - 1

    def init(self, seed):
        """
        Initialized generator with given seed.
        """

        self._previous = seed

    def rand(self):
        """
        Return next random number.
        """

        self._previous = (LCG.a * self._previous) % LCG.m
        return self._previous


if __name__ == '__main__':

    lg1 = ListRNG()
    lg1.init([45, 2, 34, 1])

    lcg1 = LCG()
    lcg1.init(346)

    fmt = "{:>10}   {:>10}"
    print fmt.format("ListRNG", "LCG")
    for _ in range(10):
        print fmt.format(lg1.rand(), lcg1.rand())
