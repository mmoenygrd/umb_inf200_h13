'''
Module providing a flattening function.
'''

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def flatten(nested):
    """
    Returns a flattened version of nested. nested must be a list.
    """

    flat = []
    for item in nested:
        if type(item) == list:
            flat.extend(flatten(item))
        else:
            flat.append(item)
    return flat

if __name__ == '__main__':

    print flatten([1, 2, 3, 4])
    print flatten([1, 2, [3, [4, 5]], [6, 7]])
    print flatten([])
