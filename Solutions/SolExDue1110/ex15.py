"""
This program finds the zero-crossing of the function provided.

The program will look for the zero in the interval given.

The program has bugs. Fix them!

See ex15_correct.py for a corrected version.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def f(x):
    """
    Returns number, f(x) assumed continuous.

    This is a sample function to be zeroed.
    """

    return 1 - x ** 2


def find_zero(func, a, b):
    """
    Returns zero point of func in [a, b].
    """

    fa, fb = func(a), func(b)
    if fa * fb > 0:
        print "Error: function has same sign at a and b."
        return None

    x = a - fa * (b - a) / (fb - fa)
    fx = func(x)

    if fa * fx > 0:
        find_zero(func, a, x)
    else:
        find_zero(func, x, b)


if __name__ == '__main__':

    print find_zero(f, 0, 2)
