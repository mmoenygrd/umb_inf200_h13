"""
Performance comparison of loop- and recursion-based Fibonacci numbers routines.

Results on MacBook Pro, 2.53 GHz i5, OSX 10.6.8, Python 2.7.5

N     Loop      Recursion
5     0.9 us       2.6 us
10    1.2 us      30.0 us
20    2.2 us    3700.0 us
40    3.3 us       ---
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def fib_loop(n):
    '''
    Returns n-th Fibonacci number.

    Code is adapted from Hetland, Beginning Python, p 115.
    '''

    if n < 2:
        return n
    else:
        n0, n1 = 0, 1
        for _ in xrange(n - 1):
            n0, n1 = n1, n0 + n1
        return n1


def fib_rec(n):
    '''
    Returns n-th Fibonacci number.

    This is a straightforward recursive implementation.
    '''

    if n < 2:
        return n
    else:
        return fib_rec(n - 1) + fib_rec(n - 2)

if __name__ == '__main__':

    TABLE_FORMAT = "{:>4}{:>12}{:>12}"
    print "Fibonacci numbers"
    print TABLE_FORMAT.format("N", "F_N (loop)", "F_N (rec)")
    for n in xrange(10):
        print TABLE_FORMAT.format(n, fib_loop(n), fib_rec(n))
