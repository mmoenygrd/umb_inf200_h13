"""
This program finds the zero-crossing of the function provided.

The program will look for the zero in the interval given.

This is a corrected version of ex15.py.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def f(x):
    """
    Returns number, f(x) assumed continuous.

    This is a sample function to be zeroed.
    """

    return 1 - x ** 2


def find_zero(func, a, b):
    """
    Returns zero point of func in [a, b].
    """
    # Fix:
    #   - Ensure arguments are float
    a, b = float(a), float(b)

    fa, fb = func(a), func(b)
    if fa * fb > 0:
        print "Error: function has same sign at a and b."
        return None

    x = a - fa * (b - a) / (fb - fa)
    fx = func(x)

    # Fixes:
    #   - Recursion now terminates (first if)
    #   - Recursion now descends into the correct subinterval
    #     (with opposite signs at both sides)
    if abs(fx) < 1e-3:
        return x
    elif fa * fx > 0:
        return find_zero(func, x, b)
    else:
        return find_zero(func, a, x)


if __name__ == '__main__':

    print find_zero(f, 0, 2)
