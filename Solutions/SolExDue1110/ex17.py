'''
Implementation and test of the middle-square random number generator.

This program collects statistics on cycle lengths for the
middle-square generator.
'''

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def middlesquare(digits, number):
    '''
    Return new "random" number with given number of digits.

    digits -- number of digits, must be even
    number -- previous random number in sequence
    '''

    assert digits % 2 == 0, "Number of digits must be even"

    # Assume digits == 4. Then the square of number will have 8 digits,
    # which we can label as
    #
    #    llnnnnrr
    #
    # Here, nnnn are the four digits of the new number, and ll and rr are
    # the digits to the left and right we need to remove.

    square = number ** 2
    halfDigits = digits / 2  # always integer, as digits is even

    # drop left, keep nnnnrr
    withoutLeft = square % 10 ** (3 * halfDigits)

    # divide to get nnnn
    newNum = withoutLeft // 10 ** halfDigits

    return newNum


def sequence_length(digits, seed):
    '''
    Return length of sequence eventually reached from given seed.

    digits -- number of digits, must be even
    seed -- initial value in sequence
    '''

    ctr = 0  # count numbers generated
    known = {}  # map each number generated to first occurance
    while seed not in known:
        known[seed] = ctr  # register first occurance of number
        seed = middlesquare(digits, seed)
        ctr += 1

    return ctr - known[seed]  # number of numbers between 1st and 2nd occurance


if __name__ == '__main__':

    stats = {}  # maps each sequence length to how often it has occured
    for n in range(10000):  # test all seeds from 0 to 9999
        sl = sequence_length(4, n)
        stats.setdefault(sl, 0)  # if sl in stats, do nothing, set to 0 else
        stats[sl] += 1  # we have seen one more seed with sequence length sl

    print stats
