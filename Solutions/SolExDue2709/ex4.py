"""
Reverse-lookup phone-book.

This program takes a pre-defined phone book, reverses it and allows lookup by number.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

if __name__ == '__main__':
    
    # Phone numbers may contain symbols, therefore they are stored as strings
    phonebook = {'Joe': '1234', 'Jane': '4321', 'Alex': '321'}
    reverseBook = dict()
    for name, number in phonebook.items():
        reverseBook[number] = name
        
    print "Reverse phonebook lookup (quit with <Enter>)"
    while True:
        number = raw_input('Number: ')
        if len(number) == 0:
            break
        elif number in reverseBook:
            print "  {} has number {}.".format(reverseBook[number], number)
        else:
            print "  The number {} is unknown.".format(number)
