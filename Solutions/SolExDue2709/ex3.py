"""
Small phone-book application.

This program allows the user to enter a given number of names and phone numbers
and to query them again later. Names cannot include spaces.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

def read_phonebook():
    """
    Returns phonebook read from standard input.
    """
    
    phonebook = {}
    numNames = int(raw_input('How many names would you like to enter: '))
    for _ in range(numNames):
        name = raw_input('Name  : ')
        number = raw_input('Number: ')
        phonebook[name] = number
        
    return phonebook


def query_phonebook(phonebook):
    """
    Lets user query phone book by name.
    
    Querying continues until user enters empty name.
    """
    
    while True:
        name = raw_input('Name or <ENTER> to quit: ').strip()
        if len(name) == 0:
            break
        elif name in phonebook:
            print "  {}'s number is {}.".format(name, phonebook[name])
        else:
            print "  {}'s number is unknown.".format(name)

if __name__ == '__main__':
    print "\nWelcome to the Phonebook Program!\n\n"
    
    print "Please enter your friends' phone numbers!"
    myPhoneBook = read_phonebook()
    
    print "\n" + "-" * 30 + "\n"
    print "You can now look up your friends' numbers by name!"
    query_phonebook(myPhoneBook)
