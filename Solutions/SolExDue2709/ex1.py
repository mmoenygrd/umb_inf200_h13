"""
Solution to exercise 1: re-ordering lists.

This program modifies lists with the most compact code I can think of.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

if __name__ == '__main__':
    
    print "\n-- Case A --"
    orig_list = list('hello')
    new_list = ['o'] + orig_list[1:]
    print new_list

    print "\n-- Case B --"
    orig_list = [1, 2, 3, 4, 5]
    new_list = orig_list[::2]
    print orig_list, new_list
    
    print "\n-- Case C --"
    orig_list = [67, 23, 54, 28, 19]
    new_list = sorted(orig_list[:1] + orig_list[2:]  + [53])
    print orig_list, new_list
