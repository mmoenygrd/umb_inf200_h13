"""
Print alphabetical list of names and (phone) numbers.

This program reads names and numbers from standard input and prints
them as an alphabetical list.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def read_phonebook():
    """
    Returns phonebook read from standard input.
    """
    
    phonebook = {}
    print "Enter name followed by number; press <ENTER> only to end entry."
    while True:
        inLine = raw_input('Name Number: ')
        if len(inLine) == 0:
            break
        
        name, number = inLine.split(' ')
        phonebook[name] = number

    return phonebook

if __name__ == '__main__':
    
    pbook = read_phonebook()
    for name in sorted(pbook.keys()):
        print "{:10}: {:>10}".format(name, pbook[name])
 