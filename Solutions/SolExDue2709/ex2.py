"""
Sample solution to exercise 2: Manipulating dictionaries

For the sake of illustration, this solution defines a function for each case.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

def print_header(label):
    "Print header line with label."
    
    print "\n-- Case {} --".format(label)
    

def caseA():
    "Remove entry with key 2."
    
    print_header('A')
    orig_dict = {0: 1, 1: 1, 2: 2, 3: 3, 4: 5}
    new_dict = orig_dict.copy()
    del new_dict[2]
    print orig_dict, new_dict


def caseB():
    "Change one entry, add one entry."
    
    print_header('B')
    orig_dict = {0: 1, 1: 1, 2: 2, 3: 3, 4: 5}
    new_dict = orig_dict.copy()
    new_dict.update({1: -1, 5: 8})
    print orig_dict, new_dict


def caseC():
    "Change one entry, also affects original."
    
    print_header('C')
    orig_dict = {0: 1, 1: 1, 2: 2, 3: 3, 4: 5}
    new_dict = orig_dict
    orig_dict[3] = 6
    print orig_dict, new_dict

if __name__ == '__main__':
    caseA()
    caseB()
    caseC()