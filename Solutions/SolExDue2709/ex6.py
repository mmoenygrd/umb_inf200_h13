"""
Company directory program.

This program contains the company employee directory providing first and last
names, phone and room numbers. Lookup is by last name, and the user can choose 
which information to obtain.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

def build_directory():
    """Returns complete company directory."""
    
    dir = {'Plesser': {'first': 'Hans', 'phone': '5467', 'room': '136'},
           'Denby': {'first': 'Cecilie', 'room': '243'},
           'Grimenes': {'first': 'Arne', 'phone': '5416', 'room': '344'}}
    
    return dir


def lookup_entry(dir, lastname):
    """Prints desired information about person with given last name."""
    
    if lastname not in dir:
        print "{} is unknown".format(lastname)
        return
    
    person = dir[lastname]
    info_available = ', '.join(sorted(person.keys()))
    info_wanted = raw_input("Please choose the information you would like ({}): "
                            .format(info_available))
    if info_wanted in person:
        print " >> {}".format(person[info_wanted])
    else:
        print " >> Information unavailable."
        

if __name__ == '__main__':
    
    print "Company Employee Directory"
    print "--------------------------\n"
    
    dir = build_directory()
    while True:
        lastname = raw_input("Please enter a last name (<ENTER> to terminate): ")
        if len(lastname) == 0:
            break
        lookup_entry(dir, lastname)