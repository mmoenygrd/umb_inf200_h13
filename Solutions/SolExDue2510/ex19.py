"""
This program finds the zero-crossing of the function provided.

The program will look for the zero in the interval given.

Loop-based.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


def f(x):
    """
    Returns number, f(x) assumed continuous.

    This is a sample function to be zeroed.
    """

    return 1 - x ** 2


def find_zero(func, a, b, eps=1e-3, return_eval_count=False):
    """
    Returns zero point of func in [a, b].

    If return_eval_count==True, the number of function evaluations
    will be returned with the solution. In this case, a tuple
    (solution, count) will be returned.
    """

    a, b = float(a), float(b)
    fa, fb = func(a), func(b)
    if fa * fb > 0:
        raise ValueError("Function has same sign at a and b.")

    eval_count = 2   # evals at f(a) and f(b)

    # get first midpoint
    x = a - fa * (b - a) / (fb - fa)
    fx = func(x)
    eval_count += 1

    while abs(fx) >= eps:
        if fa * fx > 0:
            a, fa = x, fx
        else:
            b, fb = x, fx

        # get next midpoint
        x = a - fa * (b - a) / (fb - fa)
        fx = func(x)
        eval_count += 1

    if return_eval_count:
        return x, eval_count
    else:
        return x

if __name__ == '__main__':

    message = "x_zero = {:12.8f} ({:4d} evaluations with tol = {:.0e})"
    for tol in [0.1, 0.01, 0.001, 0.0001, 0.00001]:
        x_zero, num_evals = find_zero(f, 0.5, 2, tol, True)
        print message.format(x_zero, num_evals, tol)
