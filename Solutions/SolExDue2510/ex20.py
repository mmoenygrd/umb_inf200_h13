"""
Phonebook implementation using a class for phonebook entries.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


class PhoneBookEntry(object):
    """
    Represents one entry in the phone book.

    Currently, entries contain first and last name and phone number.
    """

    def __init__(self, first, last, number):
        """
        first  - First name
        last   - Last name
        number - Phone number
        """

        self.firstName = first
        self.lastName = last
        self.number = number

    def display(self):
        """
        Print nicely formatted name and number.
        """

        print "{} {}'s number is {}".format(self.firstName,
                                            self.lastName,
                                            self.number)

if __name__ == '__main__':

    joe = PhoneBookEntry('Joe', 'Doe', '1234')
    jane = PhoneBookEntry('Jane', 'Dane', '5678')

    for entry in [joe, jane]:
        entry.display()
