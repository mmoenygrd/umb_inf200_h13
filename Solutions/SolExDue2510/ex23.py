"""
Random walk simulation

This program simulates two 1D random walkers until they meet.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

# We use the same Walker class from exercise 22
from ex22 import Walker


def getStartingPoints(nameA, nameB):
    """Returns tuple of starting points, ensuring distance is even."""

    while True:
        startA = int(raw_input("{}'s starting point: ".format(nameA)))
        startB = int(raw_input("{}'s starting point: ".format(nameB)))

        if (startA - startB) % 2 == 0:
            return startA, startB
        else:
            print "The two starting points must differ by an even number."


if __name__ == '__main__':

    startJoe, startJane = getStartingPoints('Joe', 'Jane')

    joe = Walker(startJoe)
    jane = Walker(startJane)

    while not joe.isAt(jane.getPosition()):
        joe.step()
        jane.step()

    # some logical consistency checks
    assert jane.isAt(joe.getPosition()), "Joe met Jane, but Jane not Joe."
    assert joe.getNumSteps() == jane.getNumSteps(), "Bad step count."

    message = "Jane and Joe met after {} steps. Direct distance: {}."
    print message.format(joe.getNumSteps(), abs(startJoe - startJane))
