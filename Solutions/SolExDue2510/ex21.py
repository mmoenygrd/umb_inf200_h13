"""
Phonebook implementation using a class for phonebook entries.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


class PhoneBookEntry(object):
    """
    Represents one entry in the phone book.

    Currently, entries contain first and last name and phone number.
    """

    def __init__(self, first_or_all, last=None, number=None):
        """
        Initialize phonebook entry.

        If called with three arguments, arguments must be first name,
        last name and phone number.

        If called with a single argument, it must be a string containing
        first name, last name and phone number separated by spaces.
        """

        if last is None and number is None:
            self.initFromString(first_or_all)
        else:
            if last is None:
                raise ValueError("Last name missing")
            if number is None:
                raise ValueError("Number missing")

            self.firstName = first_or_all
            self.lastName = last
            self.number = number

    def initFromString(self, data):
        """
        Initialize phonebook from string with complete information.

        Parameter:
        data - String with following format:
               "<First name> <Last name> <Number>"

        Items in data must be in correct order, separated by whitespace
        and must not in themselves contain whitespace.
        """

        self.firstName, self.lastName, self.number = data.split()

    def display(self):
        """
        Print nicely formatted name and number.
        """

        print "{} {}'s number is {}".format(self.firstName,
                                            self.lastName,
                                            self.number)


if __name__ == '__main__':

    joe = PhoneBookEntry('Joe', 'Doe', '1234')
    jane = PhoneBookEntry('Jane Dane 5678')

    for person in [joe, jane]:
        person.display()
