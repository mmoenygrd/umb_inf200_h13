"""
Gambler's ruin simulation for fair coin.

See, e.g.,
Feller, Introduction to Probability Theory and its Applications,
vol I, ch XIV.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'


import random
import numpy


class Player(object):
    """
    Represents single player.
    """

    def __init__(self, initialMoney):
        """
        initialMoney - Player's money at the beginning of the game
        """

        self.money = initialMoney

    def take_turn(self):
        """
        Simulates effect of single flip of coin.

        The player's wealth increases or decreases by one unit
        with probability 1/2.
        """

        self.money += 2 * random.randint(0, 1) - 1

    def getMoney(self):
        """Returns player's current wealth."""

        return self.money


class Game(object):
    """
    Single game between player and bank.
    """

    def __init__(self, initialPlayer, initialBank):
        """
        initialPlayer - Player's money at the beginning of the game
        initialBank - Bank's moneyat the beginning of the game

        Note:
        - The total money in the game is initialPlayer + initialBank
        - For initialPlayer == initialBank, odds for breaking the
          bank should be 50%.
        """

        self.player = Player(initialPlayer)
        self.totalMoney = initialPlayer + initialBank
        self.numTurns = 0

    def play(self):
        """
        Returns result of single game of player against bank.

        The result is a tuple (broke, turns), where
        - broke is True if the player went broke (lost everything)
        - turns is the number of turns (coin flips) in the game
        """

        while 0 < self.player.getMoney() < self.totalMoney:
            self.player.take_turn()
            self.numTurns += 1

        return self.player.getMoney() == 0, self.numTurns


class Experiment(object):
    """
    Complete experiment comprising a number of games.
    """

    def __init__(self, numGames, initialPlayer, initialBank, seed):
        """
        numGames - Number of games to simulate
        initialPlayer - Player's money at the beginning of the game
        initialBank - Bank's money at the beginning of the game
        seed - Random number generator seed
        """

        self.numGames = numGames
        self.initialPlayer = initialPlayer
        self.initialBank = initialBank
        self.brokeNumTurns = []
        self.winNumTurns = []

        random.seed(seed)

    def run(self):
        """Run experiment as defined, collecting statistics."""

        for _ in range(self.numGames):
            game = Game(self.initialPlayer, self.initialBank)
            broke, numTurns = game.play()
            if broke:
                self.brokeNumTurns.append(numTurns)
            else:
                self.winNumTurns.append(numTurns)

    def getBrokeTurns(self):
        """Returns list of duration of games the player lost."""

        return self.brokeNumTurns

    def getWinTurns(self):
        """Returns list of duration of games the player won."""

        return self.winNumTurns


if __name__ == '__main__':
    ex = Experiment(100, 50, 50, 12345)
    ex.run()
    b = ex.getBrokeTurns()
    w = ex.getWinTurns()
    print "Broke: ", len(b), min(b), numpy.mean(b), max(b)
    print "Won  : ", len(w), min(w), numpy.mean(w), max(w)
