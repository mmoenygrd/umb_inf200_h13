"""
Evaluate a mathematical function.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import math

if __name__ == '__main__':
    
    while True:
        xInput = raw_input('x: ')
        if not xInput:
            break
        
        x = float(xInput)
        if x < -10.:
            fx = x / ( x + 1 )
        elif x <= -1:
            fx = math.sin(x)
        elif x < 1:
            fx = x**2
        elif x == 1:
            fx = 20
        else:
            fx = math.log(x)
        print "f({x:g}) = {fx:g}".format(x=x, fx=fx)
        
