"""
Development of savings account tabulation.
"""

__author__ = 'Hans Ekkehard Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

INTEREST = 2.0 / 100
INCOME_TAX = 28.0 / 100
PROPERTY_TAX = 1.1 / 100
INFLATION = 1.5 /100

if __name__ == '__main__':
    
    print "Account calculator"
    
    principal = float(raw_input('Amount deposited: '))
    annualWithdrawal = float(raw_input('Annual withdrawal: '))
    numYears = int(raw_input('Number of years to calculate: '))
    
    # table header
    print
    print (('{:>4s} {:>10s} {:>10s} {:>10s}'
            + ' {:>10s} {:>10s} {:>10s}')
           .format('Year', 'Interest', 'Inc.tax', 'Prop.tax',
                   'Withdrawal', 'Principal', 'Adjusted')) 

    for year in range(numYears):
        interest = INTEREST * principal
        principal += interest
        incomeTax = INCOME_TAX * interest
        principal -= incomeTax
        propertyTax = PROPERTY_TAX * principal
        principal -= propertyTax
        withdrawal = annualWithdrawal if annualWithdrawal <= principal else principal
        principal -= withdrawal
        principalAdjusted = principal / (1+INFLATION) ** (year+1)
        
        print (("{yr:4d} {intr:10.2f} {intx:10.2f} {prtx:10.2f}"
                + " {wdrw:10.2f} {prcpl:10.2f} {prcpadj:10.2f}") 
               .format(yr=year+1, intr=interest, intx=incomeTax, prtx=propertyTax, 
                       wdrw=withdrawal, prcpl=principal, prcpadj=principalAdjusted))
        if principal <= 0:
            break
    