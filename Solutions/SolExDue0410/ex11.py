"""
Ski waxing recommendations.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

if __name__ == '__main__':
    
    print "Ski waxing recommendations"
    
    while True:
        
        tempInput = raw_input("Temperature (<ENTER> to quit): ")
        if not tempInput:
            break
        temp = float(tempInput)
        
        snowInput = raw_input("Snow type ([N]ew, [O]ld, [I]ce): ")
        
        if snowInput.upper().startswith('N'):
            if temp < -10:
                print "Green"
            elif temp < -5: 
                print "Blue"
            else:
                print "Purple"
        elif snowInput.upper().startswith('O'):
            if temp < -5:
                print "Blue extra"
            elif temp < +2:
                print "Red"
            else:
                print "Go swimming!"
        elif snowInput.upper().startswith('I'):
            if temp < 0:
                print "Blue klister"
            else:
                print "Red klister"
        else:
            print "Unknown snow type"
