"""
Propgram reorganizing lists using list comprehensions.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

if __name__ == '__main__':
    
    print "-- Case A --"
    inputLine = raw_input("Please enter some numbers: ")
    inputList = (int(item) for item in inputLine.split(' '))
    print [n for n in inputList if n % 3 == 0]
    
    print "-- Case B --"
    wordList = raw_input("Please enter some words: ").split(' ')
    print [word for word in wordList if word.lower() == word.lower()[::-1]]
    
    print "-- Case C --"
    wordList = raw_input("Please enter some words: ").split(' ')
    print [word for word in wordList 
           if word[0].upper() == 'A' and len(word) != 4]
    
    