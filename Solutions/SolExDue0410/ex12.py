"""
Selection sort.

This algorithm sorts by first going through the entire list, finding
the smallest element, and putting that at the beginning of the list.
Then it looks for the smallest element among the second and later elements,
etc.
"""
__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

if __name__ == '__main__':
    
    inLine = raw_input("Please enter some numbers: ")
    numbers = [int(item) for item in inLine.split(' ')]
 
    for j in range(len(numbers)):
        # Invariant: 
        # numbers[:j] are the j smallest numbers in numbers sorted in ascending order
        
        min_idx = j
        for k in range(j+1, len(numbers)):
            # Invariant: 
            # numbers[min_idx] <= numbers[j:k]
            if numbers[k] < numbers[min_idx]:
                min_idx = k
                
        numbers[j], numbers[min_idx] = numbers[min_idx], numbers[j]
    
    print numbers
     
        