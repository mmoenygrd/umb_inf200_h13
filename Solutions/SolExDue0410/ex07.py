"""
Sieve of Eratosthenes.

This program implements the Sieve of Erastosthenes using
a list of Booleans.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import math

if __name__ == '__main__':
    
    nMax = long(raw_input("Maximum number to test: "))

    # sieve[i] represents natural number i+1, True marks candidate prime
    sieve = [True] * nMax
    
    # For any non-prime number n <= nMax, n = j * k with min(j,k) <= sqrt(nMax)
    # Thus, we need only consider step sizes <= sqrt(nMax). We use round()
    # instead of math.floor() to avoid improper flooring in case sqrt(nMax) == m-eps
    # due to numerical error.
    maxStepSize = int(round(math.sqrt(nMax)))
    for step in range(2, maxStepSize+1):
        # Invariant: 
        # Entries in sieve for all multiples of 2..step-1 are False.
        for n in range(2*step, nMax+1, step):
            # Invariant:
            # Entries for all multiples of step smaller than n are False.  
            sieve[n-1] = False
        
    print "The prime numbers up to {} are:".format(nMax)
    primes = [n+1 for n, v in enumerate(sieve) if v]
    print primes
