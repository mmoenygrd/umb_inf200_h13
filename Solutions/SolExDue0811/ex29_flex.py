"""
Implement complete simulation experiment for walkers.

See ex29.py for a simpler solution supporting only Walker and SingleWalk.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import random
from ex22 import Walker
from ex26 import SingleWalk
from ex27 import BiasedWalker
from ex28 import TiringWalker, SingleWalkWithDeath


class Experiment(object):
    """
    Implements random walk experiment with repeated walks.
    """

    def __init__(self, WalkerClass, start, dest, numWalks, seed,
                 **walkerParams):
        """
        WalkerClass - class of walker to use
        start - initial position of walker
        dest - destination of walker (end of walk)
        numWalks - number of walks to simulate
        seed - random number generator seed
        **walkerParams - additional parameters for walker as keyword
                         parameters
        """

        self.WalkerClass = WalkerClass
        self.start = start
        self.dest = dest
        self.walkerParams = walkerParams
        self.numWalks = numWalks
        self.numSteps = None

        # We need to find out whether the walker can die and use
        # the correct SingleWalk class
        try:
            self.WalkerClass(self.start, **self.walkerParams).isDead()
        except AttributeError:
            # walker has no isDead() method
            self.WalkClass = SingleWalk
        else:
            # walker has isDead() method
            self.WalkClass = SingleWalkWithDeath

        random.seed(seed)  # initialize random number generator

    def run(self):
        """Executes experiment by running numWalk walks."""

        self.numSteps = [self.WalkClass(
                             self.WalkerClass(self.start,
                                              **self.walkerParams),
                                        self.dest).numberOfSteps()
                         for _ in range(self.numWalks)]

    def walkLengths(self):
        """Returns lengths of walks."""

        return self.numSteps


if __name__ == '__main__':

    print "Walking in different ways from 0 to 10."

    exper = Experiment(Walker, 0, 10, 10, 1234)
    exper.run()
    print "Walker 0 -> 10: ", exper.walkLengths()

    exper = Experiment(BiasedWalker, 0, 10, 10, 1234, p=0.2)
    exper.run()
    print "BiasedWalker, p=0.2: ", exper.walkLengths()

    exper = Experiment(TiringWalker, 0, 10, 10, 1234, lamb=100, qMax=0.9)
    exper.run()
    print "TiringWalker, lambda=100, qMax=0.9: ", exper.walkLengths()
