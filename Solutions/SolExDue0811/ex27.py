"""
Random walk simulation

This program simulates a random biased 1D random walk
from a starting point to a target.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import random
from ex22 import Walker
from ex26 import SingleWalk


class BiasedWalker(Walker):
    """
    Single biased walker object.

    The walker steps to the left with probability p, to the right
    with probability 1-p.
    """

    def __init__(self, x0, p):
        """
        x0 - initial position
        p  - probability of stepping left
        """

        if not (0 <= p <= 1):
            raise ValueError('Probability 0 <= p <= 1 required.')

        Walker.__init__(self, x0)
        self.p = p

    def step(self):
        """
        Change coordinate by -1 with probability p, by +1 otherwise.
        """

        self.x += -1 if random.random() < self.p else +1
        self.steps += 1


if __name__ == '__main__':

    print "Simulating biased walks from position 0 to position 2"

    for p in [0.1, 0.3, 0.5, 0.51, 0.52]:
        print "p = {:.2f}, resulting in walk durations".format(p),
        print [SingleWalk(BiasedWalker(0, p), 2).numberOfSteps()
                for _ in range(5)]
