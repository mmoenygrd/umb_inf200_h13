"""
Random walk simulation

This program simulates a random walker that slows down with time,
from a starting point to a target.

Notes:
- There was an error in the exercise: q(steps) increases with the
  number of steps taken. Therefore, the walker should die when
  q >= qMax, not when q < qMin. This is corrected here.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import random
import math
from ex22 import Walker
from ex26 import SingleWalk


class TiringWalker(Walker):
    """
    Single tiring walker object.

    The walker does not move with probability

        q = 1 - exp(-steps / lambda)

    and walks left or right with equal probability otherwise.

    When q >= qMax, the walker dies by raising a WalkerDied exception.
    """

    def __init__(self, x0, lamb, qMax):
        """
        x0 - initial position
        lamb - "time constant" describing slowdown
        qMax - for q >= qMax, the walker is considered dead
        """

        if not lamb > 0:
            raise ValueError('lambda must be strictly positive.')

        if not (0 <= qMax <= 1):
            raise ValueError('0 <= qMax <= 1 required.')

        Walker.__init__(self, x0)
        self.lamb = lamb
        self.qMax = qMax
        self.dead = False

    def step(self):
        """
        Move with probability q.
        """

        if self.dead:
            return

        q = 1 - math.exp(-self.steps / self.lamb)

        if q >= self.qMax:
            self.dead = True
            return

        if random.random() >= q:
            Walker.step(self)

    def isDead(self):
        """Returns True if Walker has died."""

        return self.dead


class SingleWalkWithDeath(SingleWalk):
    """
    Implements SingleWalk allowing for Death.
    """

    def __init__(self, walker, goal):
        SingleWalk.__init__(self, walker, goal)

    def simulate(self):
        """Simulate walk until walker dies or arrives home."""

        walker = self.walker
        while not (walker.isDead() or walker.isAt(self.goal)):
            walker.step()

    def numberOfSteps(self):
        """Returns number of steps if walker reached goal, else None."""

        walker = self.walker

        if not (walker.isDead() or walker.isAt(self.goal)):
            self.simulate()

        return walker.getNumSteps() if not walker.isDead() else None
        
        #if not walker.isDead():
        #    return walker.getNumSteps()
        #else:
        #    return None


if __name__ == '__main__':

    print "Simulating tiring walks from position 0 to position 5"

    for lamb in range(10, 100, 10):
        print "lambda = {}, resulting in walk durations".format(lamb),
        print [SingleWalkWithDeath(
                    TiringWalker(0, lamb, 0.9), 5).numberOfSteps()
               for _ in range(5)]
