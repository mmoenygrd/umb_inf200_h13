"""
The calc module provides a basic rpn calculator.

This version provides som additional mathematical functions using
closure. For a simpler approach, see ex30_calc.py.
"""

__author__ = "Hans E Plesser"
__email__ = "hans.ekkehard.plesser@umb.no"

import math
from stack import Stack


class Calc(object):
    """
    Implements a simple rpn-based calculator.

    TODO: Proper error handling.
    TODO: Convenient user interface.
    """

    def __init__(self):
        self.stack = Stack()

    def add(self):
        """Add two top numbers on stack, leave result on stack."""

        self.stack.push(self.stack.pop() + self.stack.pop())

    def sub(self):
        """Subtract topmost from second on stack, leave result on stack."""

        y = self.stack.pop()
        x = self.stack.pop()
        self.stack.push(x - y)

    def mul(self):
        """Multiply two top numbers on stack, leave result on stack."""

        self.stack.push(self.stack.pop() * self.stack.pop())

    def div(self):
        """Divide second on stack by topmost, leave result on stack."""

        y = self.stack.pop()
        x = self.stack.pop()
        self.stack.push(x / y)

    def neg(self):
        """Replace topmost by its negative value."""

        self.stack.push(-self.stack.pop())

    def inv(self):
        """Replace topmost by its multiplicative inverse."""

        self.stack.push(1 / self.stack.pop())

    def unary_function_factory(self, func):

        def unary_function(self=self, func=func):
            """Applies func to topmost element and leaves result on stack."""

            x = self.stack.pop()

            try:
                y = func(x)
            except (ArithmeticError, ValueError) as err:
                self.stack.push(x)  # some error, put argument back
                raise err  # re-raise error for handling above
            else:
                self.stack.push(y)  # calculation ok, push result

        return unary_function

    def execute_command(self, command):
        """Execute command specified as string."""

        commands = {'add': self.add,
                    'sub': self.sub,
                    'mul': self.mul,
                    'div': self.div,
                    'neg': self.neg,
                    'inv': self.inv,
                    'sin': self.unary_function_factory(math.sin),
                    'cos': self.unary_function_factory(math.cos),
                    'tan': self.unary_function_factory(math.tan),
                    'exp': self.unary_function_factory(math.exp),
                    'log': self.unary_function_factory(math.log)
                    }

        if command.strip().lower() in commands:
            commands[command]()

    def handle_input(self, user_input):
        """
        Handle input from user, either number of command.
        """

        try:
            x = float(user_input)
        except ValueError:
            self.execute_command(user_input)
        else:
            # no exception, input was number, push
            self.stack.push(x)

    def get_result(self):
        """Return result, removing it from the stack."""

        return self.stack.pop()

    def calc(self, input_line):
        """
        Returns result of calculation specified by input_line.

        Example:
        hp15.calc('24 56 add 12 sub 3 div inv neg')
        """

        for token in input_line.split():
            self.handle_input(token)

        return self.get_result()

if __name__ == "__main__":

    # this is just an example for how to use Calc, not a test setup

    hp15 = Calc()

    hp15.handle_input(56)
    hp15.handle_input('23')
    hp15.handle_input('add')
    hp15.handle_input(20)
    hp15.handle_input('add')
    hp15.handle_input(10)
    hp15.handle_input('sub')
    res = hp15.get_result()

    print "From calculator: {}, expected: {}".format(res,
                                                     56 + 23 + 20 - 10)

    hp15.handle_input(6)
    hp15.handle_input(4)
    hp15.handle_input('mul')
    print hp15.get_result()

    hp15.handle_input(24)
    hp15.handle_input(48)
    hp15.handle_input('div')
    print hp15.get_result()

    hp15.handle_input(4)
    hp15.handle_input('neg')
    print hp15.get_result()

    hp15.handle_input(4)
    hp15.handle_input('inv')
    print hp15.get_result()

    print hp15.calc('24 56 add 12 sub 3 div inv neg')

    print hp15.calc('0. sin')
    print hp15.calc('1.5 sin')
    print hp15.calc('1 sin cos exp log tan')
