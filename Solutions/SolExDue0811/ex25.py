"""
Program tabulating some mathematical functions.

The main purpose of this program is to illustrate exception handling.
"""

__author__ = "Hans Ekkehard Plesser"
__email__ = "hans.ekkehard.plesser@umb.no"


import math


def get_input():
    """Returns input as single list of tokens."""

    tokens = []
    while True:
        data = raw_input('Enter data: ')
        if not data.strip():
            break
        tokens.extend(data.split())
    return tokens


def convert_data(raw_data):
    """Returns data as one list of floats, one of non-floats."""

    numbers = []
    non_numbers = []

    for token in raw_data:
        try:
            x = float(token)
            numbers.append(x)
        except ValueError:
            non_numbers.append(token)

    return numbers, non_numbers


def tabulate(values):
    """Print table."""

    print "{:>10s}{:>10s}{:>10s}{:>10s}".format('x', 'sin(x)',
                                                'log(x)', 'sqrt(x)')
    print "-" * 40

    for x in sorted(values):

        line = "{:10.3f}".format(x)

        for func in (math.sin, math.log, math.sqrt):
            try:
                y = func(x)
                line += "{:10.3f}".format(y)
            except ValueError:
                line += "{:>10s}".format("n/a")

        print line

    print "-" * 40


if __name__ == '__main__':
    raw_data = get_input()
    nums, non_nums = convert_data(raw_data)
    tabulate(nums)
    print
    print "Non-numeric inputs: ", non_nums
