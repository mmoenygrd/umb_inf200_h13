'''
This module implements a SingleWalk class.
'''

__author__ = 'Hans E. Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

from ex22 import Walker


class SingleWalk(object):
    """
    Represents a single random walk experiment.
    """

    def __init__(self, walker, goal):
        """
        walker - Walker object
        goal - Position the walker shall reach
        """

        if walker.getNumSteps() > 0:
            raise ValueError('New Walker expected.')

        self.walker = walker
        self.goal = goal

    def simulate(self):
        """
        Let walker take steps until she is home.
        """

        while not self.walker.isAt(self.goal):
            self.walker.step()

    def numberOfSteps(self):
        """
        Returns number of steps the walker took to reach goal.
        """

        # Ensure that we have simulated
        if not self.walker.isAt(self.goal):
            self.simulate()

        return self.walker.getNumSteps()


if __name__ == '__main__':

    print "Simulating walks from position 0 to position ..."
    for goal in range(0, 10):
        print goal, "resulting in walk durations",
        print [SingleWalk(Walker(0), goal).numberOfSteps()
                for _ in range(10)]
        
    """
    for goal in range(0, 10):
        res = []
        for _ in range(10):
            walker = Walker(0)
            walk = SingleWalk(walker, goal)
            walk.simulate()
            res.append(walk.numberOfSteps())
        print res
    """