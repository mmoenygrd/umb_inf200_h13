"""
Implement complete simulation experiment for walkers.

See ex29_flex.py for a flexible solution supporting different walker
classes.
"""

__author__ = 'Hans E Plesser'
__email__ = 'hans.ekkehard.plesser@umb.no'

import random
from ex22 import Walker
from ex26 import SingleWalk


class Experiment(object):
    """
    Implements random walk experiment with repeated walks.
    """

    def __init__(self, start, dest, numWalks, seed):
        """
        start - initial position of walker
        dest - destination of walker (end of walk)
        numWalks - number of walks to simulate
        seed - random number generator seed
        """

        self.start = start
        self.dest = dest
        self.numWalks = numWalks
        self.numSteps = None

        random.seed(seed)  # initialize random number generator

    def run(self):
        """Executes experiment by running numWalk walks."""

        self.numSteps = []
        for _ in range(self.numWalks):
            walker = Walker(self.start)
            walk = SingleWalk(walker, self.dest)
            walk.simulate()
            self.numSteps.append(walk.numberOfSteps())

    def run_compact(self):
        # This function does the same as run(), but is written using
        # a list comprehension

        self.numSteps = [SingleWalk(Walker(self.start),
                                    self.dest).numberOfSteps()
                         for _ in range(self.numWalks)]

    def walkLengths(self):
        """Returns lengths of walks."""

        return self.numSteps


if __name__ == '__main__':

    print "Running some experiments walking from 0 to 10."

    for seed in [11, 11, 12, 13, 14]:
        exper = Experiment(0, 10, 10, seed)
        exper.run()
        result = exper.walkLengths()
        print "Seed: {}, Results: {}".format(seed, result)

    print
    print "Rerunning with run_compact."
    for seed in [11, 11, 12, 13, 14]:
        exper = Experiment(0, 10, 10, seed)
        exper.run_compact()
        result = exper.walkLengths()
        print "Seed: {}, Results: {}".format(seed, result)
